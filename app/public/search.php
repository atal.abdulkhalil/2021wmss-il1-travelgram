<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

$connection = getDBConnection();

$stmt = $connection->prepare('SELECT * FROM `tags` order by tags.title');
$stmt->execute([]);
$tags = $stmt->fetchAllAssociative();

if (isset($_GET['tag'])) {
    $stmt = $connection->prepare('SELECT travelgrams.id, travelgrams.title, travelgrams.description, travelgrams.added_on, authors.firstname FROM travelgrams LEFT JOIN authors ON author_id = authors.id LEFT JOIN travelgrams_to_tags ON travelgrams.id = travelgram_id LEFT JOIN tags ON travelgrams_to_tags.tag_id = tags.id WHERE tags.id = ? order by added_on DESC');
    $stmt->execute(array($_GET['tag']));
    $itemsArray = $stmt->fetchAllAssociative();
    if (! $itemsArray) {
        header('Location: /search.php');
        exit;
    }


}
echo $twig->render('pages/search-results.twig', [
    'items' => $itemsArray,
    'tags' => $tags
]);


