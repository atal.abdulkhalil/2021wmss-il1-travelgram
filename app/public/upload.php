<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

$connection = getDBConnection();
$stmt = $connection->prepare('SELECT * FROM `tags` order by tags.title');
$stmt->execute([]);
$tags = $stmt->fetchAllAssociative();


$moduleAction = (isset($_POST['moduleAction']) ? $_POST['moduleAction'] : '');
$title = (isset($_POST['title']) ? $_POST['title'] : '');
$description = isset($_POST['description']) ? (string) $_POST['description'] : '';
$taggen = isset($_POST['tags']) ? (array) $_POST['tags'] : array();
$date = date('Y-m-d H:i:s');

$errors = false;
$formErrors = [];
if ($moduleAction !== ''){
    if ($title === '') {
        $formErrors[] = 'please enter a title';
        $errors = true;
    }
    if ($description === '') {
        $formErrors[] = 'please enter a discription';
        $errors = true;
    }

   /* if (!$errors) {
        $stmt = $connection->prepare('INSERT INTO `travelgrams` (`title`, `description`, `author_id`, `added_on`) VALUES (?,?,?,?)');
        $stmt->execute([
            $_POST['title'],
            $_POST['description'],
            [2],
            ['$date']
        ]);
        header('Location: /index.php');
        exit;
    }*/
}


echo $twig->render('pages/upload.twig', [
    'tags' => $tags,
    'formErrors' => $formErrors,
    'title' => $title,
    'description' => $description
]);


