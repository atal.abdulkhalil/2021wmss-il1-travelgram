<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

$connection = getDBConnection();
$stmt = $connection->prepare('SELECT travelgrams.id, travelgrams.title, travelgrams.description, travelgrams.added_on, authors.firstname FROM
travelgrams LEFT JOIN authors ON author_id = authors.id order by added_on DESC LIMIT 2 ');
$stmt->execute([]);
$itemsArray = $stmt->fetchAllAssociative();


echo $twig->render('pages/index.twig', [
    'items' => $itemsArray
]);


